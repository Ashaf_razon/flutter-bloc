//1. TODO: imports
//2. TODO: List of employees
//3. TODO: Stream Controllers

//4. TODO: Stream Sink getter

//5. TODO: Constructor- add data; listen to changes

//6. TODO: Core Functionalities

//7. TODO: dispose

import 'dart:async';
import 'Employee.dart';

class EmployeeBloc {
  //sink to [add in pipe]
  //stream to [get data from pipe]
  //by pipe i mean data flow

  List<Employee> _employeeList = [
    Employee(1, "Emp 1", 1010.0),
    Employee(2, "Emp 2", 1010.0),
    Employee(3, "Emp 3", 4010.0),
    Employee(4, "Emp 4", 1010.0),
    Employee(5, "Emp 5", 6010.0),
    Employee(6, "Emp 6", 8010.0),
    Employee(7, "Emp 7", 10010.0),
  ];

  final _employeeListStreamController = StreamController<List<Employee>>();

  //for inc and dec
  final _employeeSalaryIncrementStreamController = StreamController<Employee>();
  final _employeeSalaryDecrementStreamController = StreamController<Employee>();

  //getters

  Stream<List<Employee>> get employeeListStream =>
      _employeeListStreamController.stream;
  StreamSink<List<Employee>> get employeeListSink =>
      _employeeListStreamController.sink;

  StreamSink<Employee> get employeeSalaryIncrement =>
      _employeeSalaryIncrementStreamController.sink;
  StreamSink<Employee> get employeeSalaryDecrement =>
      _employeeSalaryDecrementStreamController.sink;

  EmployeeBloc() {
    _employeeListStreamController.add(_employeeList);
    _employeeSalaryIncrementStreamController.stream.listen(_incrementSalary);
    _employeeSalaryDecrementStreamController.stream.listen(_decrementSalary);
  }

  _incrementSalary(Employee employee) {
    double salary = employee.salary;
    double incrementedSalary = salary * 20 / 100;

    String tempoSalary = (salary + incrementedSalary).toStringAsFixed(3);
    _employeeList[employee.id - 1].salary = double.parse(tempoSalary);
    employeeListSink.add(_employeeList);
  }

  _decrementSalary(Employee employee) {
    double salary = employee.salary;
    double decrementedSalary = salary * 20 / 100;

    String tempoSalary = (salary - decrementedSalary).toStringAsFixed(3);
    _employeeList[employee.id - 1].salary = double.parse(tempoSalary);
    employeeListSink.add(_employeeList);
  }

  void myDispose(){
    _employeeListStreamController.close();
    _employeeSalaryDecrementStreamController.close();
    _employeeSalaryIncrementStreamController.close();
  }

}