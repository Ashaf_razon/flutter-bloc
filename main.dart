import 'package:flutter/material.dart';
import 'HomePage.dart';

void main() => runApp(myApp());

class myApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Bloc App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: SizedBox(child: HomePage()),
    );
  }
}
